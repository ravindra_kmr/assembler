//NOT ABLE TO TRACK LINE NUMBER
//VRAIBLE MUST BE OF ONE CHARACTER A-Z
//SPACE MUST BE GIVEN AFTER THE END OF PROGRAM
#include<stdio.h>
#include<string.h>
#include<stdlib.h>

//initializing global variable.
//int lineno;
int g_nOutputArray[1000];
char g_chArray[100][10]; 
int g_nIndex=0;
int g_nOutputIndex=0;

//declaring all the funtions.
void ReadToken(FILE *ppointer, char chArray[100][10]);    //    function for getting the token from file to an g_chArray
int CheckVariable(char chcheck);                                
void CheckSyntax(char chArray[1000][10]);
void AssignTokenToArray(FILE *ppointer, char temperartString[10]);
int IsNotDelimiter(char chDelimiter);
int LabelCheck(char chcheck);                            //LabelCheck checking

struct Label                                             //structure for handling jump and if statment    
{
    int jumpStatement;
    int labelStatement ;
}Label[26];

void main()
{
    FILE *pfilePointer,*poutputFilePointer;
    pfilePointer = fopen("filename.txt", "r");
    
    ReadToken(pfilePointer, g_chArray);                         //reading the token from file                
    
    int i;
    for(i = 0; i < 26; i++)
    {
        Label[i].labelStatement = 0;                                //initialising the structures to be zero
        Label[i].jumpStatement = 0;
    }                          
    
    CheckSyntax(g_chArray);                                //checking the synatax of tokens
     
    for(i = 0; i < 26; i++)                //checking whether the jump statement has  their respective Label in the whole program file
    {
                                        
         if(Label[i].jumpStatement == Label[i].labelStatement)
         {
            poutputFilePointer=fopen("outputfile.txt", "w");
            //**********************writingthe code in the outputfile file************************
            for(i = 0; i < 1000; i++)
            {
                fprintf(poutputFilePointer, "%d\n", g_nOutputArray[i]);
            }
        }
        
        else
        {
             printf("Label without jump or jump without Label\n");
             exit(0);
        }
     }
     
     fclose(pfilePointer);
     fclose(poutputFilePointer);

    for(i = 0; i < g_nIndex-1; i++)
    {
        printf("%s", g_chArray[i]);
        printf("\n%d\n", g_nOutputArray[i]);
    }
}

void ReadToken(FILE *ppointer, char g_chArray[100][10])            
{	

	int i;
    char temperaryString[10];
    do                                                //for recognising the number of token
    {        
     
        i = -1;
        
        do                                        //for recognising the single token 
        {    i++;    
            temperaryString[i] = fgetc(ppointer);
            
            if(feof(ppointer))                    //condition for exit out of loop if end of file has encountered             
            {
                break;
            }
        }while(IsNotDelimiter(temperaryString[i]));        //reading the character untill delimitered is encountered
        
        i = 0;
        if(!IsNotDelimiter(temperaryString[i]))            
        {
            char buffer[100];
            if(temperaryString[i] == ':')                    //neglecting the comment
            {
                fgets(buffer, 100, ppointer);
            }
        continue;                                           //for neglecting the delimiter
        }
        
        AssignTokenToArray(ppointer, temperaryString);        //read token is assigned to g_chArray
        g_nIndex++;
    }while(!feof(ppointer));                                 //reading is continued untill end of file has been reached
/*	if(i!=1 )
	{
		AssignTokenToArray(ppointer, temperaryString); 
		
	}*/
}

int IsNotDelimiter(char chDelimiter)
{
    if((chDelimiter==' ') || (chDelimiter=='\n') || (chDelimiter==':') || (chDelimiter=='	'))
    {
        return 0;
    }
    
    else
    {
        return 1;
    }
}

void CheckSyntax(char chArray[1000][10])                    //syntaxial analysis
{
    int i;
    char chCheck;

    while(g_nOutputIndex < g_nIndex-1)
    {                                                                    //variable is not being distinguished
        if(strlen(chArray[g_nOutputIndex]) == 1)
        {                                                                
            printf("Syntax error ");                                    //error if variable is used without command
            exit(0);                
        }
    
        else if(strlen(chArray[g_nOutputIndex]) == 2)
        {
            if(!strcmp(chArray[g_nOutputIndex], "RA"))
            {        
                g_nOutputArray[g_nOutputIndex] = 1101;
                g_nOutputIndex++;
                CheckVariable(chArray[g_nOutputIndex][0]);   
            }
            
            else if(!strcmp(chArray[g_nOutputIndex], "RM"))
            {
                g_nOutputArray[g_nOutputIndex] = 1102;        
                g_nOutputIndex++;
                CheckVariable(chArray[g_nOutputIndex][0]);    
            }
            
            else if(!strcmp(chArray[g_nOutputIndex], "WM"))
            {
                g_nOutputArray[g_nOutputIndex] = 1103;
                g_nOutputIndex++;
                CheckVariable(chArray[g_nOutputIndex][0]);        
            }
            
            else if(!strcmp(chArray[g_nOutputIndex], "WA"))
            {    
                g_nOutputArray[g_nOutputIndex] = 1104;    
            }
            
            else
            {    
                printf("invalid command");
                exit(0);    
            }
        }
        
        else if(strlen(chArray[g_nOutputIndex]) == 3)
        {    
            if(!strcmp(chArray[g_nOutputIndex], "LOD"))
            {
                g_nOutputArray[g_nOutputIndex] = 1121;    
                g_nOutputIndex++;
                CheckVariable(chArray[g_nOutputIndex][0]);
            }
            
            else if(!strcmp(chArray[g_nOutputIndex], "ADD"))
            {
                g_nOutputArray[g_nOutputIndex] = 1111;
                g_nOutputIndex++;
                CheckVariable(chArray[g_nOutputIndex][0]);
            }
            
            else if(!strcmp(chArray[g_nOutputIndex], "SUB"))
            {
                g_nOutputArray[g_nOutputIndex] = 1112;                    
                g_nOutputIndex++;
                CheckVariable(chArray[g_nOutputIndex][0]);
            }
            
            else if(!strcmp(chArray[g_nOutputIndex], "DIV"))
            {
                g_nOutputArray[g_nOutputIndex] = 1113;    
                g_nOutputIndex++;
                CheckVariable(chArray[g_nOutputIndex][0]);
            }
            
            else if(!strcmp(chArray[g_nOutputIndex], "MUL"))
            {
                g_nOutputArray[g_nOutputIndex] = 1114;                    
                g_nOutputIndex++;
                CheckVariable(chArray[g_nOutputIndex][0]);
            }
            
            else if(!strcmp(chArray[g_nOutputIndex], "LBL"))
            {
                g_nOutputArray[g_nOutputIndex] = 1120;    
                g_nOutputIndex++;
                if(LabelCheck(chArray[g_nOutputIndex][0]))                                //chChecking if it is Label after the command or not
                {
                    for(i = 0; i < 26; i++)
                    {
                        if (chArray[g_nOutputIndex][0] == ('a' + i))
                        {                
                                 Label[i].labelStatement = 7878;            //magic number
                                g_nOutputArray[999-26-i] = g_nOutputIndex +2 ;          //giving the tooken no.of Label    
                        }
                    }
                }
                
                else
                {
                    exit(0);
                }
            }
            
            else if(!strcmp(chArray[g_nOutputIndex], "ASS"))
            {
                g_nOutputArray[g_nOutputIndex] = 1123;    
            }
            
            else if(!strcmp(chArray[g_nOutputIndex], "INC"))
            {
                g_nOutputArray[g_nOutputIndex] = 1131;
            }
            
            else if(!strcmp(chArray[g_nOutputIndex], "DEC"))
            {
                g_nOutputArray[g_nOutputIndex] = 1132;        
            }
            
            else if(!strcmp(chArray[g_nOutputIndex], "JLZ"))
            {    
                g_nOutputArray[g_nOutputIndex]=1134;
                g_nOutputIndex++;
                
                if(LabelCheck(chArray[g_nOutputIndex][0]))
                {
                    for(i = 0; i < 26 ; i++)
                    {
                        if (chArray[g_nOutputIndex][0] == ('a'+i))
                        {
                            Label[i].jumpStatement = 7878;                    
                        }
                    }
                }                
            }
            
            else if(!strcmp(chArray[g_nOutputIndex], "JGZ"))
            {    
                g_nOutputArray[g_nOutputIndex] = 1133;
                if(LabelCheck(chArray[g_nOutputIndex][0]))
                {
                    for(i = 0; i < 26; i++)
                    {
                        if (chCheck == ('a'+i))
                        {
                            Label[i].jumpStatement = 7878;
                        }
                    }
                }
            }
            
            else if(!strcmp(chArray[g_nOutputIndex], "JNZ"))
            {    
                g_nOutputArray[g_nOutputIndex] = 1134;
                if(LabelCheck(chArray[g_nOutputIndex][0]))
                {
                    for(i = 0; i < 26; i++)
                    {
                        if (chCheck == ('a'+i))
                        {
                            Label[i].jumpStatement = 7878;
                        }
                    }
                }
            }
            
            else if(!strcmp(chArray[g_nOutputIndex], "JEZ"))
            {    
                g_nOutputArray[g_nOutputIndex] = 1135;
                if(LabelCheck(chArray[g_nOutputIndex][0]))
                {
                    for(i = 0; i < 26; i++)
                    {
                        if (chCheck == ('a'+i))
                        {
                            Label[i].jumpStatement = 7878;
                        }
                    }
                }
            }
            else
            {
                printf("invalid command");
                exit(0);    
            }
        }
        g_nOutputIndex++;
    }
}

int LabelCheck(char chCheck)
{
    int i;
	int nLabelAddress = 0;
    
	for(i = 0; i < 26; i++)
    {
        if (chCheck == ('a'+i))
        {
            nLabelAddress = (999-26-i);
        }
    }
    
	g_nOutputArray[g_nOutputIndex]=nLabelAddress;                      //assigning the value to hte corresponding variable                      
    
    if(nLabelAddress == 0)
    {
        printf("invalid variable");
        exit(0);    
    }
    return 1;
}

int CheckVariable(char chCheck)
{
    int i;
	int nVariableAddress=0;
    for (i = 0; i < 26; i++)
    {
        if (chCheck == 'A'+i)
        {
            nVariableAddress = 999-i;
        }
    }
     g_nOutputArray[g_nOutputIndex] = nVariableAddress;
                                        
    if(nVariableAddress == 0)
    {
        printf("invalid  variable");
        exit(0);
        return 0;
}
    return 1;
}

void AssignTokenToArray(FILE *ppointer, char temperartString[10])
    {
        int i = 0;
        char chBuffer[100];
                
        while(IsNotDelimiter(temperartString[i]))
        {
           if(temperartString == ":")                                                   //for removing the line of comment in a program
            {
                fgets(chBuffer, 100, ppointer);
                break;
            }
            g_chArray[g_nIndex][i] = temperartString[i];                                        //example R:comment ,in this R is stored as token and others is deleted
            i++;
        }
    }
